## pyEQUIB
**pyEQUIB** - Python package for atomic level populations and line emissivities in statistical equilibrium

The **pyEQUIB** package is a collection of [Python](https://www.python.org/) programs developed to calculate atomic level populations and line emissivities in statistical equilibrium in multi-level atoms for different physical conditions of the stratification layers where the chemical elements are ionized. This library includes the Python implementation of the program [EQUIB](http://adsabs.harvard.edu/abs/2016ascl.soft03005H), which was originally written in FORTRAN by [Howarth & Adams (1981)](http://adsabs.harvard.edu/abs/1981ucl..rept.....H), and was recently converted to Python. It also includes the Python implementation of deredden functions from [STSDAS IRAF Package](http://www.stsci.edu/institute/software_hardware/stsdas), hydrogen emissivities ([Osterbrock & Ferland 2006](http://adsabs.harvard.edu/abs/2006agna.book.....O); [Storey & Hummer 1995](http://adsabs.harvard.edu/abs/1995yCat.6064....0S)), helium emissivities ([Smits 1996](http://adsabs.harvard.edu/abs/1996MNRAS.278..683S); [Porter et al. 2012](http://adsabs.harvard.edu/abs/2012MNRAS.425L..28P)), heavy element recombination emissivities of C II ([Davey et al. 2000 ](http://adsabs.harvard.edu/abs/2000A%26AS..142...85D)), N II ([Escalante & Victor 1990](http://adsabs.harvard.edu/abs/1990ApJS...73..513E)), O II ([Storey 1994](http://adsabs.harvard.edu/abs/1994A%26A...282..999S); [Liu et al. 1995](http://adsabs.harvard.edu/abs/1995MNRAS.272..369L)), Ne II ([Kisielius et al. 1998](http://adsabs.harvard.edu/abs/1998A%26AS..133..257K)), and C III and N III ([Pequignot et al. 1991](http://adsabs.harvard.edu/abs/1991A%26A...251..680P)) using the Python implementation of the [MIDAS](http://www.eso.org/~ohainaut/ccd/midas.html) scripts by X. W. Liu.

Website: [physics.mq.edu.au/~ashkbiz/pyequib](http://physics.mq.edu.au/~ashkbiz/pyequib/)

### References
* A. Danehkar, Q.A. Parker & W. Steffen, [AJ, 151, 38, 2016](http://adsabs.harvard.edu/abs/2016AJ....151...38D)

* A. Danehkar, H. Todt, B. Ercolano & A.Y. Kniazev, [MNRAS, 439, 3605, 2014](http://adsabs.harvard.edu/abs/2014MNRAS.439.3605D)

* A. Danehkar, Q.A. Parker & B. Ercolano, B. [MNRAS, 434, 1513, 2013](http://adsabs.harvard.edu/abs/2013MNRAS.434.1513D)

* A. Danehkar, Ph.D. Thesis, [Macquarie University, 2014](http://adsabs.harvard.edu/abs/2014PhDT........76D)
