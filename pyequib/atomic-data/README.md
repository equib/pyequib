## Atomic Data

The atomic data are from the [CHIANTI atomic database](http://www.chiantidatabase.org/) version 5.2, and have been used in [MOCASSIN v2.0](https://github.com/mocassin/MOCASSIN-2.0).

