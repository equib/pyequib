## pyEQUIB
[![PyPI version](https://badge.fury.io/py/pyequib.svg)](https://badge.fury.io/py/pyequib)
[![Build Status](https://travis-ci.org/equib/pyEQUIB.svg?branch=master)](https://travis-ci.org/equib/pyEQUIB)
[![Coverage Status](https://coveralls.io/repos/github/equib/pyEQUIB/badge.svg?)](https://coveralls.io/github/equib/pyEQUIB?branch=master)
[![Binder](http://mybinder.org/badge.svg)](http://mybinder.org/repo/equib/pyequib)
[![GitHub license](https://img.shields.io/aur/license/yaourt.svg)](https://github.com/equib/pyEQUIB/blob/master/LICENSE)

Python package for atomic level populations and line emissivities in statistical equilibrium

### Description
The **pyEQUIB** package is a collection of [Python](https://www.python.org/) programs developed to calculate atomic level populations and line emissivities in statistical equilibrium in multi-level atoms for different physical conditions of the stratification layers where the chemical elements are ionized. This library includes the Python version of the program [EQUIB](http://adsabs.harvard.edu/abs/2016ascl.soft03005H), which was originally written in Fortran by [Howarth & Adams (1981)](http://adsabs.harvard.edu/abs/1981ucl..rept.....H), and was recently converted to Python.

Website: [physics.mq.edu.au/~ashkbiz/pyequib](http://physics.mq.edu.au/~ashkbiz/pyequib/)

### Installation
To install the last version, all you should need to do is

    python setup.py install

To install the stable version, you can use the preferred installer program (pip):

    pip install pyequib

### References

* A. Danehkar, Q.A. Parker & W. Steffen, [AJ, 151, 38, 2016](http://adsabs.harvard.edu/abs/2016AJ....151...38D)

* A. Danehkar, H. Todt, B. Ercolano & A.Y. Kniazev, [MNRAS, 439, 3605, 2014](http://adsabs.harvard.edu/abs/2014MNRAS.439.3605D)

* A. Danehkar, Q.A. Parker & B. Ercolano, [MNRAS, 434, 1513, 2013](http://adsabs.harvard.edu/abs/2013MNRAS.434.1513D)

* A. Danehkar, Ph.D. Thesis, [Macquarie University, 2014](http://adsabs.harvard.edu/abs/2014PhDT........76D)
